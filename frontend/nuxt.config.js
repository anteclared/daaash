/* eslint-disable no-param-reassign */
const path = require('path');
const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = {
  head: {
    title: 'daaash',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'daaash - Dashboard for tech people',
      },
    ],
    link: [
      {
        rel: 'stylesheet',
        href: 'https://use.typekit.net/svq4dat.css',
      },
    ],
  },
  loading: false,
  css: [
    'normalize.css',
    '@fortawesome/fontawesome-svg-core/styles.css',
    '@/scss/global.scss',
  ],
  modules: ['nuxt-sass-resources-loader'],
  sassResources: [
    '@/scss/abstracts/_variables.scss',
    '@/scss/abstracts/_functions.scss',
    '@/scss/abstracts/_mixins.scss',
  ],
  watchers: {
    webpack: {
      poll: true,
    },
  },
  build: {
    extend (config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
        config.plugins.push(
          new StyleLintPlugin({
            configFile: path.resolve(__dirname, '.stylelintrc.json'),
            syntax: 'scss',
            files: ['**/*.scss', '**/*.vue'],
            lintDirtyModulesOnly: ctx.isDev,
          }),
        );
      }

      config.resolve = config.resolve || {};
      config.resolve.alias = config.resolve.alias || {};
      config.resolve.alias.PublicCSSAbstracts = '~/assets/css/abstracts';
    },
  },
  plugins: ['~/plugins/font-awesome'],
};
