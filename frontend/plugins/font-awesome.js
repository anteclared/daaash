import Vue from 'vue';
import { config, library } from '@fortawesome/fontawesome-svg-core';
import {
  faArrowAltCircleDown,
  faSquare,
  faCheckSquare,
  faRocket,
} from '@fortawesome/pro-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

config.autoAddCss = false;

library.add(faArrowAltCircleDown, faSquare, faCheckSquare, faRocket);

Vue.component('BaseIcon', FontAwesomeIcon);
