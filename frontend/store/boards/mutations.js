/* eslint no-param-reassign: 0 */
import * as S from 'sanctuary';
import * as types from './mutation-types';

const getById = id => S.find(S.pipe([S.prop('id'), S.equals(id)]));
const getWidget = ({ boards, boardId, widgetId }) => {
  const { widgets = [] } = S.fromMaybe({})(getById(boardId)(boards));
  return getById(widgetId)(widgets);
};

export default {
  [types.WIDGET_TODOS_MARK_TODO_COMPLETED] (
    state,
    { todoId, boardId, widgetId },
  ) {
    const widget = S.fromMaybe({})(
      getWidget({ boards: state.boards, boardId, widgetId }),
    );
    const toDo = S.fromMaybe({})(getById(todoId)(widget.data.todos));

    toDo.completed = true;
  },
  [types.WIDGET_TODOS_MARK_TODO_UNCOMPLETED] (
    state,
    { todoId, boardId, widgetId },
  ) {
    const widget = S.fromMaybe({})(
      getWidget({ boards: state.boards, boardId, widgetId }),
    );
    const toDo = S.fromMaybe({})(getById(todoId)(widget.data.todos));

    toDo.completed = false;
  },
};
