import mutations from './mutations';

const boardsModule = {
  state: {
    boards: [
      {
        id: '28041531-a0f8-4a8f-995f-2512f2363d4b',
        config: {
          columnsCount: 5,
        },
        widgets: [
          {
            id: 'a753623b-668c-414d-a59f-836f533162f8',
            boardId: '28041531-a0f8-4a8f-995f-2512f2363d4b',
            type: 'rss-feed',
            config: {
              column: 1,
              title: 'Smashing Magazine',
              feeds: [
                {
                  id: '31ae0c98-de10-42db-8972-3a748b3e6f78',
                  url: 'http://rss1.smashingmagazine.com/feed/',
                  title: 'SmashingMagazine',
                },
              ],
              largeArticlesCount: 1,
            },
            data: {
              articles: [
                {
                  feedId: '31ae0c98-de10-42db-8972-3a748b3e6f78',
                  title: 'How To Get To Know Your Users',
                  content: `Users are at the heart of User-Centered Design (UCD),
                  with designers focusing on actual users and their needs
                  throughout the design process.`,
                  url:
                    'https://www.smashingmagazine.com/2018/06/how-to-get-to-know-your-users/',
                  timestamp: '1530124981',
                  image: 'https://picsum.photos/300/300?image=392',
                },
                {
                  feedId: '9b2c30eb-7b66-419c-89fb-7212e2668115',
                  title: `Everything You Need To Know About Transactional Email
                  But Didn’t Know To Ask`,
                  url:
                    'https://www.smashingmagazine.com/2018/06/guide-transactional-email/',
                  timestamp: '1530121381',
                  image: 'https://picsum.photos/300/300?image=292',
                },
                {
                  feedId: '87cbae1f-7a66-4368-94ae-752f14e4bfc7',
                  title: 'How To Get To Know Your Users',
                  url:
                    'https://www.smashingmagazine.com/2018/06/how-to-get-to-know-your-users/',
                  timestamp: '1530117781',
                  image: 'https://picsum.photos/300/300?image=232',
                },
                {
                  feedId: 'abc8c265-271a-4e97-9049-a9fbbfc060a4',
                  title:
                    'What Newsletters Should Designers And Developers Be Subscribing To?',
                  url:
                    'https://www.smashingmagazine.com/2018/06/newsletters-for-designers-and-developers/',
                  timestamp: '1530042181',
                  image: 'https://picsum.photos/300/300?image=178',
                },
              ],
            },
          },
          {
            id: 'd39125b4-ef38-4e38-bc01-7ede8124b928',
            boardId: '28041531-a0f8-4a8f-995f-2512f2363d4b',
            type: 'todos',
            config: {
              column: 2,
              title: 'My To-dos',
            },
            data: {
              todos: [
                {
                  id: '70d1ffdc-307b-4656-866d-7946b035c8ca',
                  title: 'Ping client about that last payment',
                  timestamp: '1530124681',
                  completed: false,
                },
                {
                  id: '7a5668c1-ae84-4cb3-aee5-cf83e99c99a4',
                  title: 'Eat something cooked',
                  url: 'https://www.daaash.com',
                  timestamp: '1530124981',
                  completed: false,
                },
                {
                  id: '51782e7e-fd97-497b-b81e-0d6de9d17ff0',
                  title: 'Shave!!',
                  url: 'https://www.daaash.com',
                  timestamp: '1530121381',
                  completed: false,
                },
                {
                  id: 'b526feab-cf69-4279-9310-31005b692b4b',
                  title: 'Check out this slick board',
                  url: 'https://www.daaash.com',
                  timestamp: '1530121881',
                  completed: true,
                },
              ],
            },
          },
        ],
      },
    ],
  },
  mutations,
};

export default boardsModule;
