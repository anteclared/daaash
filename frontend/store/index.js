import Vuex from 'vuex';
import boardsModule from './boards';
import mobileMenuModule from './mobile-menu';

const createStore = () => new Vuex.Store({
  modules: {
    mobileMenu: mobileMenuModule,
    boards: boardsModule,
  },
});

export default createStore;
