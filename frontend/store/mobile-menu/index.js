import mutations from './mutations';

const mobileMenuModule = {
  state: {
    isOpen: false,
  },
  mutations,
};

export default mobileMenuModule;
