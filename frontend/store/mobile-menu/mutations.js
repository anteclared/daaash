/* eslint no-param-reassign: 0 */
import * as types from './mutation-types';

export default {
  [types.TOGGLE_MOBILE_MENU] (state) {
    state.isOpen = !state.isOpen;
  },
  [types.OPEN_MOBILE_MENU] (state) {
    state.isOpen = true;
  },
  [types.CLOSE_MOBILE_MENU] (state) {
    state.isOpen = false;
  },
};
