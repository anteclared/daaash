const mixin = {
  props: {
    boardId: {
      type: String,
      default: '',
    },
    widgetId: {
      type: String,
      default: '',
    },
  },
  computed: {
    board () {
      return this.$store.state.boards.boards.find(
        board => board.id === this.boardId,
      );
    },
    widget () {
      return this.board.widgets.find(widget => widget.id === this.widgetId);
    },
  },
};

export default mixin;
