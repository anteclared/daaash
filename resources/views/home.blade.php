<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

    <title>SentrySpace</title>

    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Main Font -->
    <script src="js/webfontloader.min.js"></script>

    <script>
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.css">

    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="css/main.min.css">
    <link rel="stylesheet" type="text/css" href="css/fonts.min.css">
</head>

<body class="landing-page">

<div class="content-bg-wrap">
    <div class="content-bg"></div>
</div>


<!-- Header Standard Landing  -->

<div class="header--standard header--standard-landing" id="header--standard">
    <div class="container">
        <div class="header--standard-wrap">

            <a href="#" class="logo">
                <div class="img-wrap">
                    <img src="img/logo.png" alt="SentrySpace">
                    <img src="img/logo-colored-small.png" alt="SentrySpace" class="logo-colored">
                </div>
                <div class="title-block">
                    <h6 class="logo-title">SentrySpace</h6>
                    <div class="sub-title">SECURE SOCIAL NETWORK</div>
                </div>
            </a>
        </div>
    </div>
</div>

<!-- ... end Header Standard Landing  -->
<div class="header-spacer--standard"></div>

<div class="container">
    <div class="row display-flex">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="landing-content">
                <h1>Welcome to the Most Secure Social Network in the World</h1>
                <p>We are the most secure social network. All your data is private, not even we know about it. Read
                    more!</p>
                <a href="#" class="btn btn-md btn-border c-white">Register Now!</a>
            </div>
        </div>

        <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-xs-12">

            <!-- Login-Registration Form  -->

            <div class="registration-login-form">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#register" role="tab">
                            <svg class="olymp-login-icon">
                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-login-icon"></use>
                            </svg>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#profile" role="tab">
                            <svg class="olymp-register-icon">
                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-register-icon"></use>
                            </svg>
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="register" role="tabpanel" data-mh="log-tab">
                        <div class="title h6">Register to SentrySpace</div>
                        <form class="content">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Your Email</label>
                                        <input id="uuid" class="form-control" placeholder="" type="email">
                                    </div>

                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Your Password</label>
                                        <input class="form-control" placeholder="" type="password">
                                    </div>

                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Your Password Again</label>
                                        <input class="form-control" placeholder="" type="password">
                                    </div>

                                    <div class="remember">
                                        <div class="checkbox">
                                            <label>
                                                <input name="optionsCheckboxes" type="checkbox">
                                                I accept the <a href="#">Terms and Conditions</a> of the website
                                            </label>
                                        </div>
                                    </div>

                                    <a href="#" class="btn btn-purple btn-lg full-width">Complete Registration!</a>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane" id="profile" role="tabpanel" data-mh="log-tab">
                        <div class="title h6">Login to your Account</div>
                        <form class="content">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Your Email</label>
                                        <input class="form-control" placeholder="" type="email">
                                    </div>
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">Your Password</label>
                                        <input class="form-control" placeholder="" type="password">
                                    </div>

                                    <div class="remember">
                                        <a href="#" class="forgot">Forgot my Password</a>
                                    </div>

                                    <a href="#" class="btn btn-lg btn-primary full-width">Login</a>

                                    <p>Don’t you have an account? <a href="#register">Register Now!</a> it’s really simple and
                                        you can start enjoing all the benefits!</p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- ... end Login-Registration Form  -->        </div>
    </div>
</div>


<!-- JS Scripts -->
<script src="js/jquery-3.2.1.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/perfect-scrollbar.js"></script>
<script src="js/jquery.matchHeight.js"></script>
<script src="js/svgxuse.js"></script>
<script src="js/imagesloaded.pkgd.js"></script>
<script src="js/Headroom.js"></script>
<script src="js/velocity.js"></script>
<script src="js/ScrollMagic.js"></script>
<script src="js/jquery.waypoints.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/material.min.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/smooth-scroll.js"></script>
<script src="js/selectize.js"></script>
<script src="js/swiper.jquery.js"></script>
<script src="js/moment.js"></script>
<script src="js/daterangepicker.js"></script>
<script src="js/simplecalendar.js"></script>
<script src="js/fullcalendar.js"></script>
<script src="js/isotope.pkgd.js"></script>
<script src="js/ajax-pagination.js"></script>
<script src="js/Chart.js"></script>
<script src="js/chartjs-plugin-deferred.js"></script>
<script src="js/circle-progress.js"></script>
<script src="js/loader.js"></script>
<script src="js/run-chart.js"></script>
<script src="js/jquery.magnific-popup.js"></script>
<script src="js/jquery.gifplayer.js"></script>
<script src="js/mediaelement-and-player.js"></script>
<script src="js/mediaelement-playlist-plugin.min.js"></script>
<script defer src="fonts/fontawesome-all.js"></script>

<script src="js/bootstrap.bundle.js"></script>
<script src="js/sodium.min.js"></script>
<script src="js/crypto.js"></script>
<script src="js/user.js"></script>
<script src="js/openpgp.min.js"></script>

</body>
</html>