<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return 'hi';
});


/**
 * OAuth Routes
 */
Route::get('/login/github', 'Auth\GithubController@redirectToProvider');
Route::get('/login/github/callback', 'Auth\GithubController@handleProviderCallback');

Route::get('/login/bitbucket', 'Auth\BitbucketController@redirectToProvider');
Route::get('/login/bitbucket/callback', 'Auth\BitbucketController@handleProviderCallback');

Route::get('/login/google', 'Auth\GoogleController@redirectToProvider');
Route::get('/login/google/callback', 'Auth\GoogleController@handleProviderCallback');

Route::get('/login/facebook', 'Auth\FacebookController@redirectToProvider');
Route::get('/login/facebook/callback', 'Auth\FacebookController@handleProviderCallback');

Route::post('/user/logout', 'UserController@logout');
Route::post('/user/delete', 'UserController@delete');
Route::get('/user/status', 'UserController@status');
Route::post('/user/setName', 'UserController@setName');