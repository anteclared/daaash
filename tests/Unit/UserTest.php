<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\User;

class UserTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations;

    public function testSlugExists()
    {
        factory(User::class)->create(['slug' => 'my-slug']);

        $user = new User();

        $this->assertTrue($user->slugExists('my-slug'));
        $this->assertFalse($user->slugExists('my-slug2'));
    }

    public function testGenerateSlug()
    {
        factory(User::class)->create(['slug' => 'my-slug']);
        factory(User::class)->create(['slug' => 'sluggy']);
        factory(User::class)->create(['slug' => 'sluggy1']);


        $user = new User();

        $this->assertEquals('my-slug1', $user->generateSlug('my-slug'));
        $this->assertEquals('other-slug', $user->generateSlug('other-slug'));
        $this->assertEquals('sluggy2', $user->generateSlug('sluggy'));
    }
}

