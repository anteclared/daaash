<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\User;

class UserControllerTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations;

    public function testLogout()
    {
        $this->withSession(['user' => []])
            ->postJson('/user/logout')
            ->assertSessionMissing('user');
    }

    public function testDeleteNotLoggedIn()
    {
        $this->flushSession();
        $result = $this->postJson('/user/delete');
        $result->assertJson(['success' => false]);
    }

    public function testDeleteWeirdUserId()
    {
        $result = $this->withSession(['user' => ['id' => -1]])
            ->postJson('/user/delete');

        $result->assertJson(['success' => false]);
    }

    public function testDeleteLoggedInWithProperUserId()
    {
        factory(User::class)->create(['id' => 1]);

        $result = $this->withSession(['user' => ['id' => 1]])
            ->postJson('/user/delete');

        $result->assertJson(['success' => true]);
    }

    public function testGetUserStatusNotLoggedIn()
    {
        $result = $this->getJson('/user/status');

        $result->assertJson(['loggedIn' => false]);
    }

    public function testGetUserStatusLoggedIn()
    {
        $session = [
            'user' => [
                'id' => 1,
                'email' => 'test@test.com',
                'username' => [
                    'name' => 'Johnny Test',
                    'slug' => 'johnny-test'
                ]
            ]
        ];

        $result = $this->withSession($session)
            ->getJson('/user/status');

        $result->assertJson(['loggedIn' => true, 'user' => $session['user']]);
    }

    public function testSetName()
    {
        factory(User::class)->create(['id' => 1, 'slug' => 'sluggy']);
        factory(User::class)->create(['id' => 2, 'slug' => 'existing-slug']);
        $session = [
            'user' => [
                'id' => 1,
                'email' => 'test@test.com',
                'username' => [
                    'name' => 'Sluggy',
                    'slug' => 'sluggy'
                ]
            ]
        ];

        $this->withSession($session)
            ->postJson('/user/setName', ['name' => 'Johnny Test'])
            ->assertJson(['slug' => "johnny-test"])
            ->assertSessionHas('user.username.name', 'Johnny Test')
            ->assertSessionHas('user.username.slug', 'johnny-test');


        $this->withSession($session)
            ->postJson('/user/setName', ['name' => 'Sluggy'])
            ->assertJson(['slug' => 'sluggy']);

        $this->withSession($session)
            ->postJson('/user/setName', ['name' => 'Existing Slug'])
            ->assertJson(['slug' => 'existing-slug1'])
            ->assertSessionHas('user.username.name', 'Existing Slug')
            ->assertSessionHas('user.username.slug', 'existing-slug1');
    }

}
