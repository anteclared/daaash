<?php

namespace Tests\Unit;

use App\Http\Controllers\Auth\AuthControllerAbstract;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\User;
use Laravel\Socialite\AbstractUser;

class AuthControllerAbstractTester extends AuthControllerAbstract
{

    public function redirectToProvider()
    {
        return;
    }

    public function handleProviderCallback()
    {
        return;
    }

    public function createUserIfNeededAndLogin(AbstractUser $userData) {
        parent::createUserIfNeededAndLogin($userData);
    }
}

class AbstractUserTester extends AbstractUser
{

}

class AuthControllerAbstractTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations;

    public function testCreateUserIfNeededAndLoginNoUserCreated()
    {
        // create user so we have the mock table to test with
        factory(User::class)->create();

        $user = new AbstractUserTester();
        $user->email = 'test@test.com';
        $user->name = 'Johnny Test';
        $user->token = '';
        $user->refreshToken = '';

        $authControllerAbstractTester = new AuthControllerAbstractTester();
        $authControllerAbstractTester->createUserIfNeededAndLogin($user);

        $this->assertDatabaseHas('users', [
            'email' => $user->email,
            'name' => $user->name,
            'slug' => 'johnny-test'
        ]);
    }
}
