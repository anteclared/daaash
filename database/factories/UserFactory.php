<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'id' => $faker->unique()->numerify('###'),
        'name' => $faker->name,
        'slug' => $faker->slug,
        'email' => $faker->unique()->safeEmail,
        'access_token' => 'abc',
        'refresh_token' => null
    ];
});
