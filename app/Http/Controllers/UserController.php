<?php
namespace App\Http\Controllers;

use App\User;

class UserController extends Controller
{
    public function logout()
    {
        if (session()->isStarted()) {
            session()->flush();
        }
    }

    private function isLoggedIn()
    {
        return session()->isStarted() && session()->has('user');
    }

    public function delete()
    {
        if (!$this->isLoggedIn()) {
            return response()->json(['success' => false]);
        }

        $success = false;

        $userData = session()->get('user');

        $userId = $userData['id'];

        $user = User::query()->find($userId);

        if (!$user) {
            $success = false;
        } else {
            $success = $user->delete();
        }


        return response()->json(['success' => $success]);
    }

    public function status()
    {
        $loggedIn = $this->isLoggedIn();

        $result = [
            'loggedIn' => $loggedIn
        ];

        if ($loggedIn) {
            $result['user'] = session()->get('user');
        }

        return response()->json($result);
    }

    public function setName()
    {
        if (!$this->isLoggedIn()) {
            return response()->json(['slug' => null]);
        }

        $userData = session()->get('user');
        $name = request()->post('name');

        $user = User::query()
            ->where('id', '=', $userData['id'])
            ->first();

        $user->name = $name;
        $slug = $user->generateSlug($name);
        $user->slug = $slug;
        $user->save();
        $user->saveSession();

        return response()->json(['slug' => $slug]);
    }
}