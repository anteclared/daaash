<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class BitbucketController extends AuthControllerAbstract
{
    /**
     * Redirect the user to the Bitbucket authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('bitbucket')
            ->scopes(['account', 'email'])
            ->redirect();
    }

    /**
     * Obtain the user information from Bitbucket.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('bitbucket')->user();

        $this->createUserIfNeededAndLogin($user);
    }
}