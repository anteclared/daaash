<?php

namespace App\Http\Controllers\Auth;

use Laravel\Socialite\AbstractUser;
use App\User;
use App\Http\Controllers\Controller;

abstract class AuthControllerAbstract extends Controller
{
    abstract public function redirectToProvider();

    abstract public function handleProviderCallback();

    /**
     * @param AbstractUser $userData
     * @return $this
     */
    protected function createUserIfNeededAndLogin(AbstractUser $userData)
    {
        $user = User::query()->where('email', $userData->getEmail())->first();

        if (!$user) {
            $user = new User();
            $user->name = $userData->getName();
            $user->slug = $user->generateSlug($user->name);
            $user->email = $userData->getEmail();
        }

        $user->access_token = $userData->token;
        $user->refresh_token = $userData->refreshToken;
        $user->save();
        $user->saveSession();

        return redirect()->away(env('OAUTH_REDIRECT_URL', 'http://www.daaash.com'))->sendHeaders();
    }
}