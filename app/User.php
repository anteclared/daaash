<?php

namespace App;

use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    public $timestamps = true;

    /**
     * @var Slugify
     */
    private static $slugify;

    public function __construct(array $attributes = [])
    {
        static::$slugify = new Slugify();
        parent::__construct($attributes);
    }

    /**
     * - Converts name to slug (Cocur\Slugify library)
     * - Checks if slug exists (slugExists())
     *      - If it does not exist, returns the slug
     *      - If it does exist, loops through adding 1 to original slug every time until we find one
     *
     * @param $name
     * @return string the slug generated
     */
    public function generateSlug($name)
    {
        $originalSlug = static::$slugify->slugify($name);

        if ($this->slug == $originalSlug) {
            return $this->slug;
        }

        $slug = $originalSlug;

        $counter = 1;

        do
        {
            $slugExists = $this->slugExists($slug);

            if ($slugExists) {
                $slug = $originalSlug . $counter++;
            }
        }

        while($slugExists);

        return $slug;
    }

    public function slugExists($slug)
    {
        $count = $this->newQuery()
            ->where('slug', '=', $slug)
            ->count();

        return $count > 0;
    }

    public function saveSession()
    {
        session()->put([
            'user' => [
                'id' => $this->id,
                'email' => $this->email,
                'username' => [
                    'name' => $this->name,
                    'slug' => $this->slug
                ]
            ]
        ]);
    }
}
