$dev_aliases = <<-SCRIPT
    sudo cp /var/www/html/.dev_aliases.sh /home/vagrant/.bash_aliases
    sudo echo "cd /var/www/html" >> /home/vagrant/.profile
SCRIPT

$php_extensions = <<-SCRIPT
    docker exec php-fpm docker-php-ext-install pdo pdo_mysql
    docker exec php-fpm pecl install xdebug
    docker exec php-fpm docker-php-ext-enable xdebug
    docker restart php-fpm
SCRIPT

$db_migration = <<-SCRIPT
    docker exec php-fpm php /var/www/html/artisan migrate
SCRIPT

$requirements = <<-SCRIPT
    docker run --rm --name composer -v /var/www/html:/var/www/html -w /var/www/html composer:latest composer install
    docker exec node npm install
SCRIPT

Vagrant.configure("2") do |config|
  config.vm.provider "virtualbox" do |v|
    v.memory = 4096
    v.cpus = 4
  end

  config.vm.box = "ubuntu/xenial64"

  config.vm.synced_folder ".", "/var/www/html", mount_options: ["dmode=775,fmode=777"], owner: "vagrant",group: "www-data"

  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
  config.vm.network "private_network", type: "dhcp"
  config.vm.network "private_network", ip: "192.168.3.2"
  config.vm.provision :docker
  config.vm.provision :docker_compose, yml: "/var/www/html/docker-compose.yml", run: "always", rebuild: false
  config.vm.provision "shell", inline: $php_extensions, run: 'once'
  config.vm.provision "shell", inline: $dev_aliases, run: 'once'
  config.vm.provision "shell", inline: $requirements, run: 'once'
  config.vm.provision "shell", inline: $db_migration, run: 'once'
end
